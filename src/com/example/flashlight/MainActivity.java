package com.example.flashlight;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.example.flashlight.exceptions.HardwareException;
import com.example.flashlight.flashlightcontroller.FlashlightController;
import com.example.flashlight.util.Util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends Activity {

    /**
     * Flashlight switch button.
     */
    private Button flashlightButton = null;

    /**
     * Logger.
     */
    private final Logger log = Logger.getLogger(MainActivity.class.getName());

    /**
     * Flashlight controller.
     */
    private FlashlightController flashlightController = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        flashlightButton = (Button) findViewById(R.id.flashlightButton);
        try {
            flashlightController = new FlashlightController(this);
            flashlightButton.setOnClickListener(new FlashlightButtonClickListener());
        } catch (HardwareException e) {
            flashlightButton.setEnabled(false);
            log.log(Level.INFO, e.getMessage(), e);
            Util.showAlertDialog(this, e.getMessage());
        }
    }

    /**
     * Called when the activity is being destroyed. Performs releasing of application resources.
     */
    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            if (flashlightController != null) {
                flashlightController.close();
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Class to process flashlight button clicks.
     */
    private class FlashlightButtonClickListener implements OnClickListener {

        /**
         * OnClick method implementation.
         *
         * @param v view
         */
        public void onClick(View v) {
            boolean active = !flashlightController.active();
            flashlightController.setLight(active);
            int buttonColor = getResources().getColor(active ? R.color.COLOR_LIGHTS_ON : R.color.COLOR_LIGHTS_OFF);
            flashlightButton.setBackgroundColor(buttonColor);
        }
    }
}
