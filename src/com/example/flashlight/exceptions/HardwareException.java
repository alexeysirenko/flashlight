package com.example.flashlight.exceptions;

/**
 * Hardware exception.
 */
public class HardwareException extends Exception {

    /**
     * Creates class instance.
     *
     * @param e parent exception
     */
    public HardwareException(Exception e) {
        super(e);
    }

    /**
     * Creates class instance.
     *
     * @param message exception message
     */
    public HardwareException(String message) {
        super(message);
    }
}
