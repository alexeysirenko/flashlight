package com.example.flashlight.util;

import android.app.Activity;
import android.app.AlertDialog;

/**
 * Useful class with handy methods.
 */
public class Util {

    /**
     * Shows alert dialog.
     *
     * @param activity parent activity
     * @param message dialog message
     */
    public static void showAlertDialog(Activity activity, String message) {
        AlertDialog dialog = new AlertDialog.Builder(activity).create();
        dialog.setMessage(message);
        dialog.show();
    }
}
