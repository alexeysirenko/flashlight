package com.example.flashlight.flashlightcontroller;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import com.example.flashlight.R;
import com.example.flashlight.exceptions.HardwareException;

import java.io.Closeable;
import java.io.IOException;

/**
 * Flashlight controller. Allows toggle device flashlight.
 */
public class FlashlightController implements Closeable {

    /**
     * Activity to invoke methods
     */
    private final Activity activity;

    /**
     * Flashlight status.
     */
    private boolean active = false;

    /**
     * Device camera.
     */
    private Camera camera = null;

    /**
     * Creates class instance, opens device camera resource.
     *
     * @param activity application activity, not null
     * @throws HardwareException in case of device does not have a flashlight or camera is unreachable
     */
    public FlashlightController(Activity activity) throws HardwareException {
        this.activity = activity;
        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            throw new HardwareException(this.activity.getResources().getString(R.string.ERROR_FLASHLIGHT_NOT_FOUND));
        }
        camera = Camera.open();
        if (camera == null) {
            throw new HardwareException(this.activity.getResources().getString(R.string.ERROR_CAMERA_UNREACHABLE));
        }
        camera.stopPreview();
    }

    /**
     * Turns flashlight on and off.
     *
     * @param active turn flashlight on
     */
    public void setLight(boolean active) {
        Camera.Parameters p = camera.getParameters();
        if (active) {
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(p);
            camera.startPreview();
        } else {
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(p);
            camera.stopPreview();
        }
        this.active = active;
    }

    /**
     * Returns is flashlight active.
     *
     * @return is flashlight active
     */
    public boolean active() {
        return active;
    }

    /**
     * Releases device camera resource.
     *
     * @throws IOException
     */
    public void close() throws IOException {
        camera.release();
    }

}
